const express = require("express");
const messages = require('./app/messages');

const app = express();
app.use(express.json());

const port = 8080;
const path = '/messages';

app.use(path, messages);

app.listen(port, () => {
  console.log(`Server is live on ${port} port`);
})