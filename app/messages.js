const express = require("express");
const router = express.Router();
const fs = require('fs');
const pathDB = './DB/messages';

router.get('/', (req, res) => {
  fs.readdir(pathDB, (err, files) => {
    files.splice(0, files.length - 5);
    const resBody = [];
    if (err){
      res.status(404).send(err);
    } else {
      files.forEach(file => {
        resBody.push(JSON.parse(fs.readFileSync(pathDB + '/' + file, {encoding: 'utf8'})));
      })
      res.send(resBody);
    }
  })
})

router.post('/', (req, res) => {
  const resBody = req.body;
  resBody.time = new Date().toISOString();
  fs.writeFile('./DB/messages/' + resBody.time + '.txt', JSON.stringify(req.body), (err) => {
    if (err) {
      console.log(err);
      res.status(400).send(err);
    } else {
      res.send(resBody);
    }
  })
})

module.exports = router;